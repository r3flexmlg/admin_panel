<?php
session_start();
require("ap_config.php");

//if (!isset($_POST['password'])) return;

if ($_POST['password'] == $password) {
    $_SESSION['auth'] = 1;
    echo 1;
}

if (isset($_POST['logout'])) {
    session_destroy();
    echo 1;
}

if ($_POST['function'] == "Get_Virtualserver_Session_ID") {
    if (isset($_COOKIE['virtualserver_id'])) {
        echo json_encode($_COOKIE['virtualserver_id']);
    } else {
        echo 0;
    }
}
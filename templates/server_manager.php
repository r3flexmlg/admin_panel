<div class="card card-custom">
<div class="card-body">
    <button class="btn" id='button' onClick = 'serverAction()'>Button</button>
    <button class="btn btn-info" onClick = 'restart_server()'>Restart Server</button>
    <input type="message" id="stop_server_message" class="form-control form-control-lg" placeholder="Stop Server Message"></input>
</div>
</div>

<div class="card card-custom">
<div class="card-body">
    <?php include("gm.html"); ?>
</div>
</div>

<div class="card card-custom">
<div class="card-body">
    <?php include("select_ts3_server.html"); ?>
</div>
</div>

<div class="card card-custom">
<div class="card-body">
    <?php include("server_load.html"); ?>
</div>
</div>

<style>

#stop_server_message {
    margin-top: 15px;
}

.card-custom {
    box-shadow: 0 2px 3px hsla(0,0%,4%,.1), 0 0 0 1px hsla(0,0%,4%,.1);
    padding: 1.5rem;
    border-radius: 8px !important;
}

.form-control::placeholder {
    color: #5C5C5C !important;
}

.form-control {
    background: white !important;
    display: inline !important;
    width: 80%;
    margin-right: 10px;
}

</style>

<script>
    // On page load, check server status to adjust button properly.
    update_action_button();

    // Make start/stop/restart request.
    function serverAction() {
    
    var action = document.getElementById("button").value;

    if (action == "Stop") {
        stop_server();
    } else if (action == "Start") {
        start_server();
    }

    update_action_button();

    }

    // Restart
    function restart_server() {
        stop_server();
        update_action_button();
        start_server();
        update_action_button();
    }

function stop_server(msg = null) {
    var msg = document.getElementById("stop_server_message").value;
    var request = $.ajax({
        async: false,
        url: api_url,
        type: 'post',
        data: { 
            "function": "Stop Server",
            "msg": msg
            },
    });

    request.done( function ( response ) {
        response = JSON.parse(response);
        console.log(response);
        alertify.set('notifier','position', 'top-center');
        if (response.success == true) {
            // Success codes.
            alertify.notify("Successfully stopped server", 'notify_success', 3);
        
        } else if (response.success == false) {
            // Fail codes.
            var errors = response.errors;
            errors.forEach(alertify_notify_error);

        }
    });

    request.fail( function ( jqXHR, textStatus) {
        alertify.error('Sorry: ' + textStatus);
    });   
     
}

function start_server() {

var request = $.ajax({
    async: false,
    url: api_url,
    type: 'post',
    data: { 
        "function": "Start Server"
        },
});

request.done( function ( response ) {
    response = JSON.parse(response);
    console.log(response);
    alertify.set('notifier','position', 'top-center');
    if (response.success == true) {
        // Success codes.
        alertify.notify("Successfully started server", 'notify_success', 3);
        
    } else if (response.success == false) {
        // Fail codes.
        var errors = response.errors;
        errors.forEach(alertify_notify_error);

    }
});

request.fail( function ( jqXHR, textStatus) {
    alertify.error('Sorry: ' + textStatus);
});   
 
}

function get_server_status() {
    var resp;

    var request = $.ajax({
    async: false,
    url: api_url,
    type: 'post',
    data: { 
        "function": "Server_Status"
        },
    });

    request.done( function ( response ) {
        response = JSON.parse(response);
        resp = response.data.virtualserver_status;
    });

    request.fail( function ( jqXHR, textStatus) {
        alertify.error('Sorry: ' + textStatus);
    });

    return resp;  
}

function update_action_button() {
    var server_status = get_server_status();
    var btn = document.getElementById("button");

    if (server_status == "online") {
        $('#button').html("Stop Server");
        $('#button').val("Stop");     
        btn.classList.remove("btn-success");
        btn.classList.add("btn-danger");

    } else {
        $('#button').html("Start Server");
        $('#button').val("Start"); 
        btn.classList.remove("btn-danger");
        btn.classList.add("btn-success");
    }
}

function alertify_notify_success(item, index) {
    alertify.notify(item, 'notify_success', 3);
}

</script>
<div class="row" style="margin-bottom: 2rem;">

    <div class="col-md-8 client-manager-main">
      <select class="custom-select mr-sm-2 option_font_large" id="select_client" onChange="populate_table()">
        <option value="default_option" disabled selected>Select Client</option>
      </select>
      <?php include("kick.html"); ?>
      <?php include("ban.html"); ?>
      <?php include("message.html"); ?>
    </div>

    <div class="col-md-4">
    <label class="mr-sm-2" for="sort_option">Sort by</label>
      <select class="custom-select mr-sm-2" id="sort_option" onChange="sort_table()">
        <option selected>Default</option>
        <option value="1">Alphabetical</option>
        <option value="2">Status</option>
      </select>
    </div>

</div>

<div class="col-md-12">
<table class="table" id="table">
  <thead>
    <tr>
      <th scope="col">Icon</th>
      <th scope="col">Server Group</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody id="sg_table_body">
  </tbody>
</table>
</div>

<style>
    .form-control::placeholder {
      color: #5C5C5C !important;
    }

    .form-control {
      background: white !important;
      display: inline !important;
      width: 80%;
      margin-right: 10px;
      color: black;
    }

    .card-custom {
        box-shadow: 0 2px 3px hsla(0,0%,4%,.1), 0 0 0 1px hsla(0,0%,4%,.1);
        padding: 1.5rem;
        border-radius: 8px !important;
        margin-bottom: 2rem;
    }

    .option_font_large {
        font-size: large;
    }

    #kick_button {
      margin-right: 5px;
    }
</style>

<script>
  var online = false;
// var obj = {};
// var handler = {
//   set(target, key, value) {
//     // Only if current online value is different than previous - only detects changes
//     if (target[key] == value) return;
//     //console.log(`Setting value ${key} as ${value}`)
//     target[key] = value;
//     if (target[key]) {
//       // Create new event
//       let event = new Event("client_online");
//       document.dispatchEvent(event);
//     } else {
//       // Create new event
//       let event = new Event("client_offline");
//       document.dispatchEvent(event);
//     }
//   },
// };

// var online = new Proxy(obj, handler);
populate_select_client();

function populate_select_client() {
var online_clients;

var request = $.ajax({
    async: false,
    url: api_url,
    type: 'post',
    data: { 
      "function": 'Get_Client_List'
    },
});

request.done( function (response) {
  online_clients = JSON.parse(response);
});

request.fail( function ( jqXHR, textStatus) {
    console.log( 'Sorry: ' + textStatus );
});

var request = $.ajax({
    url: api_url,
    type: 'post',
    data: { 
      "function": 'Get_DB_Client_List'
    },
});

request.done( function (response) {
  var clients = JSON.parse(response);
  // Filtering non online clients.
  clients = clients.filter(function(client) {
    var not_intersecting = 1;
    online_clients.filter(function(online_client) {
      if (online_client.client_database_id == client.cldbid) not_intersecting = 0;
    });
    return not_intersecting;
  });

  online_clients.sort(function(a, b){
      // Alphabetic sort
      //compare two values
      if(a.client_nickname.toLowerCase() < b.client_nickname.toLowerCase()) return -1;
      if(a.client_nickname.toLowerCase() > b.client_nickname.toLowerCase()) return 1;
      return 0;
  });

  clients.sort(function(a, b){
      // Alphabetic sort
      //compare two values
      if(a.client_nickname.toLowerCase() < b.client_nickname.toLowerCase()) return -1;
      if(a.client_nickname.toLowerCase() > b.client_nickname.toLowerCase()) return 1;
      return 0;
  });
  
  var select = document.getElementById("select_client");
  var last_selected_client = select.options[ select.selectedIndex ];
  if (last_selected_client.value == "default_option") {
    select.options.length = 1;
  } else {
    select.options.length = 0;
  }
  
  for(key in online_clients) {
    option = document.createElement('option');
    option.setAttribute('value', online_clients[key].client_database_id);
    option.appendChild(document.createTextNode(online_clients[key].client_nickname));
    option.style.color = "#008000";
    option.id = "online";
    select.appendChild(option);
  }

  for(key in clients) {
    option = document.createElement('option');
    option.setAttribute('value', clients[key].cldbid);
    option.appendChild(document.createTextNode(clients[key].client_nickname));
    //option.className = "option_offline";
    option.style.color = "#ff5050";
    select.appendChild(option);
  }

  var select = document.getElementById("select_client");
  var options = select.options
  var previous_online_status = online;
  for(option in options) {
    // Look for last selected client
    if (select[option].value == last_selected_client.value) {
      // Select client.
      select[option].selected = true;
      // Inherit color from its option.
      select.style.color = select[option].style.color;
      if (select[option].id == "online") {
        if (online == false) client_online();
        online = true;
      } else {
        if (online == true) client_offline();
        online = false;
      }
    }
  }
  //var last_selected_client = select.options[ select.selectedIndex ].value;
  setTimeout(populate_select_client, 1000*5);
});

request.fail( function ( jqXHR, textStatus) {
    console.log( 'Sorry: ' + textStatus );
});
}

function populate_table() {
var select = document.getElementById("select_client");
// remove default select option after choosing option
if (select[0].value == "default_option") select.remove(0);
var client_dbid = select.options[ select.selectedIndex ].value;
var child_color = select.options[ select.selectedIndex ].style.color;
select.style.color = child_color;
//populate_select_client();
if (select.options[ select.selectedIndex ].id == "online") {
  if (online == false) client_online();
  online = true;
} else {
  if (online == true) client_offline();
  online = false;
}
var server_groups;
var client_server_groups;

var request = $.ajax({
    async: false,
    url: api_url,
    type: 'post',
    data: { 
      "function": 'Get_Client_Server_Groups',
      "client_dbid": client_dbid
    },
});

request.done( function (response) {
  client_server_groups = JSON.parse(response); 
});

request.fail( function ( jqXHR, textStatus) {
    console.log( 'Sorry: ' + textStatus );
});

var request = $.ajax({
    async: false,
    url: api_url,
    type: 'post',
    data: { 
      "function": 'Get_Server_Groups'
    },
});

request.done( function (response) {
  server_groups = JSON.parse(response);
});

request.fail( function ( jqXHR, textStatus) {
    console.log( 'Sorry: ' + textStatus );
});

// start populating table
var myTable = document.getElementById("sg_table_body");
var rowCount = myTable.rows.length;
for (var x=rowCount-1; x>=0; x--) {
   myTable.deleteRow(x);
}

var table = document.getElementById("sg_table_body");

for(key in server_groups) {

  var btn_class = "btn btn-success";
  var html = "Add";
  var action = "Add";

  var button;

  for(keyi in client_server_groups) {
    if (client_server_groups[keyi].sgid == server_groups[key].sgid) {
      btn_class = "btn btn-danger";
      html = "Remove";
      action = "Remove";
    }
  }

  var row = table.insertRow(-1);
  var cell1 = row.insertCell(0);
  var cell2 = row.insertCell(1);
  var cell3 = row.insertCell(2);

  cell2.innerHTML = server_groups[key].name;
  cell2.value = server_groups[key].sgid;

  button = document.createElement("button");
  button.innerHTML = html;
  button.className = btn_class;
  button.onclick = (function (element) {
      return function () {
        action_sg(element);
      };
    })(row);

  cell3.appendChild(button);
  cell3.value = action;
}
  var select = document.getElementById("sort_option");
  var sort = select.options[ select.selectedIndex ].innerHTML;
  if (sort != "Default") sort_table();
  append_Icons();
}

function action_sg(element)
{
  var select = document.getElementById("select_client");
  var client_dbid = select.options[ select.selectedIndex ].value;
  var sgid = element.cells[1].value;
  var action = element.cells[2].value;

  if (action == "Add") {
    var opposite_class = "btn btn-danger";
    var opposite_html = "Remove";
    var opposite_action = "Remove";
  } else {
    var opposite_class = "btn btn-success";
    var opposite_html = "Add";
    var opposite_action = "Add";
  }

  var request = $.ajax({
    url: api_url,
    type: 'post',
    data: { 
      "function": (action == "Add") ? "Add_SG" : "Remove_SG",
      "client_dbid": client_dbid,
      "sgid": sgid
    },
  });

  request.done( function (response) {
    element.cells[2].childNodes[0].className = opposite_class;
    element.cells[2].childNodes[0].innerHTML = opposite_html;
    element.cells[2].value = opposite_action;
    var select = document.getElementById("sort_option");
    var sort = select.options[ select.selectedIndex ].innerHTML;
    if (sort != "Default") sort_table();
    populate_select_client();
  });

  request.fail( function ( jqXHR, textStatus) {
      console.log( 'Sorry: ' + textStatus );
  });

} 

function sort_table() {
  var select = document.getElementById("sort_option");
  var sort = select.options[ select.selectedIndex ].innerHTML;
  var table = document.getElementById("sg_table_body");
  var rows = table.rows;
  var switching = true;

  if (sort == "Alphabetical") {
  while (switching) {
    switching = false;
    for (i = 0; i < (rows.length-1); i++) {
      var a = rows[i].cells[1].innerHTML;
      var b = rows[i+1].cells[1].innerHTML;
      if (a < b) {
        table.insertBefore(table.rows[i+1], table.rows[i]);
        switching = true;
        continue;
      }
    }
  }

  } else if (sort == "Status") {
    while (switching) {
      switching = false;
      for (i = 0; i < (rows.length-1); i++) {
        var a = rows[i].cells[2].value;
        var b = rows[i+1].cells[2].value;
        if (a < b) {
          table.insertBefore(table.rows[i+1], table.rows[i]);
          switching = true;
          continue;
        }
      }
    }
    // After status sort, apply alphabetical sort with respecting status sort
    while (switching) {
      switching = false;
      for (i = 0; i < (rows.length-1); i++) {
        var a = rows[i].cells[1].innerHTML;
        var b = rows[i+1].cells[1].innerHTML;
        if (a < b) {
          table.insertBefore(table.rows[i+1], table.rows[i]);
          switching = true;
          continue;
        }
      }
    }

  } else if (sort == "Default") {
    populate_table();
  }
}

function append_Icons() {
  var request = $.ajax({
      url: api_url,
      type: 'post',
      data: { 
        "function": "Get_All_Server_Group_Icons"
      },
  });

  request.done( function (response) { 
    var sg_icons = JSON.parse(response);
    // Append icons to table
    var table = document.getElementById("sg_table_body");
    var rows = table.rows;
    for (let i = 0; i < rows.length; i++) {
      const sgid = rows[i].cells[1].value;
      var icon = sg_icons[sgid];
      if (!icon) icon = sg_icons["null_img"];
      var img = document.createElement("IMG");
      rows[i].cells[0].appendChild(img);
      img.width = "16";
      img.height = "16";
      img.src = "data:image/jpg;base64," + icon;
    }
  });

  request.fail( function ( jqXHR, textStatus) {
    console.log( 'Sorry: ' + textStatus );
  });
}

  // catch on document...
  function client_online() {
    var client_manager_div = document.querySelector(".client-manager-main");
    // Kick button
    var template = document.querySelector('#kick_template');
    var clone = document.importNode(template.content, true);
    var btn = clone.querySelector('#kick_button');
    client_manager_div.appendChild(btn);
    // Message button
    template = document.querySelector('#message_template');
    clone = document.importNode(template.content, true);
    btn = clone.querySelector('#message_button');
    client_manager_div.appendChild(btn); 
  }

  // catch on document...
  function client_offline() {
    var el = document.querySelector("#kick_button");
    el.remove();
    el = document.querySelector("#message_button");
    el.remove();
  }
</script>
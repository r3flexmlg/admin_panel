<div class="container">
<div class="rule-body">
<div class="col-md-8 offset-md-2">

<div class="form-group">
  <input type="password" class="form-control" id="password" placeholder="Password">
  <button type="button" id = "button" class="btn btn-primary" onClick = 'login()'>Submit</button>
</div>

</div>
</div>
</div>

<style>
.form-control::placeholder {
    color: #5C5C5C !important;
  }

  .form-control {
    background: white !important;
    display: inline !important;
    width: 80%;
    margin-right: 10px;
    margin-top: 10px;
    margin-bottom: 10px;
  }
</style>

<script>
function login() {
    var password = document.getElementById("password").value;
    var request = $.ajax({
        url: 'auth.php',
        type: 'post',
        data: { 
        "password": password
        },
    });

    request.done( function ( response ) {
        alertify.set('notifier','position', 'top-center');
        if (response == 1) {
          alertify.notify("Logged in!", 'notify_success', 3);
          window.location.replace(host + "/teamspeak/admin_panel");
        } else {
          alertify_notify_error("Sorry Wrong Password");
        }
    });

    request.fail( function ( jqXHR, textStatus) {
        console.log( 'Sorry: ' + textStatus );
    });
}

// Get the input field
var input = document.getElementById("password");

// Execute a function when the user releases a key on the keyboard
input.addEventListener("keyup", function(event) {
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    document.getElementById("button").click();
  }
});

</script>
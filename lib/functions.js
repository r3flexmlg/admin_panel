// Global variables section.
var api_url = "api_functions.php";
class Ban { 
    numeric;
    type;
    format;
    reason;
    recipient;
}

class Message { 
    type;
    content;
    recipient;
}

class Kick { 
    reason;
    recipient;
}
                
function alertify_notify_error(item, index) {
	alertify.notify(item, 'notify_error', 3);
}
<?php
include("../librerias/ts3admin.class.php");
require('../configuraciones/ts3.php');

use par0noid\ts3admin as ts3;

#build a new ts3admin object
$tsAdmin = new ts3($HOST_QUERY, $PORT_QUERY);

if($tsAdmin->getElement('success', $tsAdmin->connect())) {
	#login as serveradmin
	$tsAdmin->login($USER_QUERY, $PASS_QUERY);
} else {
	//echo 'Connection could not be established.';
}

?>

<nav class = "nav-panel sticky-top" id = "nav-panel">
    <div class="container" id="nav_cont">
        <!-- Id of 6 is reserved! do not apply id of 6 to any of these elements! - will broke logout button -->
        <button id="1" class="button-panel">Server Manager</button>
        <button id="3" class="button-panel">Client Manager</button>
        <button id="4" class="button-panel">Ban List</button>
        <button id="5" value="logout_btn" class="button-panel float-right" onClick="logout_btn_toggle()"><img src="../login.png" width="16" height="16" alt="submit"/></button>
    </div>
</nav>

    <div class="container">
    <div class="rule-body">
        <div class="col-md-12">
            <div class = "panel" style="padding-bottom: 20px;"></div>
        </div>
    </div>
    </div>

<style>
    .panel {
        padding-top: 20px;
    }

    .float-right {
        float: right;
    }

    .nav-panel {
        font-size: 0;
        background-color: #0071dc;
        overflow: hidden;
        padding-top: 0px !important;
        padding-bottom: 0px !important;
    }

    .button-panel {
        display: inline-block;
        border: none;
        padding: 1rem 1rem;
        margin: 0;
        text-decoration: none;
        background: #0071dc;
        color: #ffffff;
        font-family: sans-serif;
        font-size: 1rem;
        line-height: 1;
        cursor: pointer;
        text-align: center;
        transition: background 250ms ease-in-out, transform 150ms ease;
        -webkit-appearance: none;
        -moz-appearance: none;
    }

    .nav-panel button:hover {
        background-color: #2695ff;
    }

    .active {
        background-color: #004688;
    }

    .random_border {
        border-bottom: #ffb100;
        border-width: 5px;
        border-bottom-style: solid;
    }

</style>

    <script>
    // Get all buttons with class="btn" inside the container
    var btns = document.getElementsByClassName("button-panel");
        for (var i = 0; i < btns.length; i++) {
            // logout button, dont register event listener for toggling button color.
            //if (i == 4) continue;
            btns[i].addEventListener("click", function() {
                var current = document.getElementsByClassName("active");
                var unload = false;
                 // If there's no active class
                if (current.length > 0) {
                    if (current[0].id != this.id) {
                        if (this.id == 5) {
                            let logout = document.getElementById(5);
                            logout.classList.toggle("active");
                            return;
                        };
                        current[0].className = current[0].className.replace(" active", "");
                    } else {
                        unload = true;
                    }
                }
                // if currenty active button is different than, currently clicked btn,
                // do not make currently clicked btn active.
                //if (add_active) this.className += " active";
                if (this.value != "logout_btn") sessionStorage.last_tab = this.id;
                this.classList.toggle("active");

                if (!unload) {         
                switch (this.id) {
                case '1':
                    $( ".panel" ).load( "templates/server_manager.php" );
                    break;
                case '3':
                    $( ".panel" ).load( "templates/client_manager.php" );
                    break;
                case '4':
                    $( ".panel" ).load("templates/ban_list.html");
                    break;
                case '5':
                    //$( ".panel" ).load( "admin_panel/logout.php" );
                    break;
                default:
                    console.log('Sorry, we are out of ' + expr + '.');
                }

                } else {
                    document.getElementsByClassName("panel")[0].innerHTML = "";
                }
            });
        }

        function test() {
            var button = event.srcElement;
            for (let index = 0; index < 5; index++) {
                var btn = document.getElementById(index+1);
                //.classList.remove("mystyle");  
            }
            //button.classList.add("mystyle");
        }

    function logout() {
        // make ajax call to logout.
        var request = $.ajax({
        url: 'auth.php',
        type: 'post',
        data: { 
        "logout": "true"
        },
        });

        request.done( function (response) {
            if (response == 1) window.location.replace(host + "/teamspeak/admin_panel");
        });

        request.fail( function ( jqXHR, textStatus) {
            console.log( 'Sorry: ' + textStatus );
        });
    }

    function logout_btn_toggle() {
        var button = event.srcElement;
        var btn = document.getElementById(6);
        var navbar = document.getElementById("nav_cont");
        if (btn) {
            // remove this button
            btn.remove();
            //document.getElementById(1).click();
        } else {
            // add this button
            var btn = document.createElement("BUTTON");   // Create a <button> element
            btn.innerHTML = "Logout";
            btn.id = 6;
            btn.classList.add("button-panel");
            btn.classList.add("logout-btn");
            btn.style = "float: right;";
            btn.addEventListener("click", logout, false); 
            var place;
            for (let i = 0; i < navbar.childNodes.length; i++) {
                if (navbar.childNodes[i].id == 5) place = i;
                
            }
            navbar.insertBefore(btn, navbar.childNodes[place]);  
        }
    }

    if (sessionStorage.last_tab) {
        document.getElementById(sessionStorage.last_tab).click();
    } else {
        document.getElementById(1).click();
    }
</script>
<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	session_start();
	include("ap_config.php");
?>

	<!DOCTYPE html>

	<html>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/alertify.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/css/alertify.css"></link>
	<!-- Tooltips  -->
	<script src="https://unpkg.com/popper.js@1"></script>
	<script src="https://unpkg.com/tippy.js@5"></script>

	<script src="lib/functions.js"></script>
	<link rel="stylesheet" href="lib/style.css"/></link>

	<link rel="stylesheet" href="https://unpkg.com/tippy.js@5/dist/backdrop.css" />
	<?php require_once ("../../IkarosWebsite/header.php"); ?>

    <body>

	<?php require_once ("../../IkarosWebsite/nav.php"); ?>

		<!-- LOGO -->
		<div class="container">
			<nav class="navbar navbar-toggleable-md navbar-inverse">
				<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<a class="navbar-brand" href="https://comunidad.universalgg.com/"><img src="../../assets/img/logo.png"></a>
			</nav>
		</div>

		<!-- TÉRMINOS -->	
		<div class="rules">	
		<?php
			if (!isset($_SESSION['auth'])) {
				include("login.php");
			} elseif (isset($_SESSION['auth']) && !isset($_COOKIE['virtualserver_id'])) {
				?>
				<div class="container">
				<div class="rule-body">
				<div class="col-md-8 offset-md-2">
				<?php include("templates/select_ts3_server.html"); ?>
				</div>
				</div>
				</div>
			<?php
			} else {
				include("panel.php");
			}	
		?>
		</div>
			<?php require_once ("../../IkarosWebsite/footer.php"); ?>
			
			<script>
			var host = "<?php echo $host; ?>";

			</script>
					<!-- SCRIPTS -->
					<!-- Encriptadas -->
					<script src="../../assets/js/jquery/3.2.1/jquery.min.js"></script>
					<script src="../../assets/js/bootstrap.min.js"></script>
					<script src="../../assets/js/encriptamiento/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
					<script src="../../assets/js/script.min1b26.js?v2"></script>

					<!-- Analíticas de Google -->
					<script async src="https://www.googletagmanager.com/gtag/js?id=UA-126576194-1"></script>
					<script>
						window.dataLayer = window.dataLayer || [];
						function gtag(){dataLayer.push(arguments);}
						gtag('js', new Date());

						gtag('config', 'UA-126576194-1');
					</script>


					<!-- Cookies -->
					<script src="../../assets/js/cookies.js"></script>

					<script type="text/javascript">

						window.onload = function(){

							if ( document.getElementById('showModal') ){

								alert('Debes Estar Conectado al servidor.');
							}

						}

					</script>
				</body>
			</html>
		<?php //} ?>

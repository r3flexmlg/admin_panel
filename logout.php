<div class="col-md-6">
    <button type="button" id = "button" class="btn btn-primary" onClick = 'logout()'>Logout</button>
</div> 

<script>

function logout() {
    var request = $.ajax({
        url: 'auth.php',
        type: 'post',
        data: { 
        "logout": "true"
        },
    });

    request.done( function (response) {
        if (response == 1) window.location.replace(host + "/teamspeak/admin_panel");
    });

    request.fail( function ( jqXHR, textStatus) {
        console.log( 'Sorry: ' + textStatus );
    });

}

</script>
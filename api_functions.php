<?php
//ini_set('display_errors','on');
ini_set('session.gc_maxlifetime', 5);
session_start();
// check if user is logged in.
if (!isset($_SESSION['auth'])) return;

include('../includes/path_config.php');
include('ap_config.php');
include('connect.php');
include(ICON_CACHE_PATH);

if (!isset($_POST['function'])) return;
   
    if ($_POST['function'] == 'Get_Server_List') {
        $servers_list = $tsAdmin->serverList();
        echo json_encode($servers_list);
    }

    if ($_POST['function'] == 'Set_Virtualserver_Session_ID') {
        if (!isset($_POST['virtualserver_id'])) return;
        $virtualserver_id = $_POST['virtualserver_id'];
        setcookie('virtualserver_id', $virtualserver_id, time()+36000*14, '/');
        echo 1;
        return;
    }

// Check if user had selected virtual server id.
if (!isset($_COOKIE['virtualserver_id'])) return;

    $tsAdmin->selectServer($_COOKIE['virtualserver_id'], "serverId");
    //$server_status = $tsAdmin->serverList()['data'][0]['virtualserver_status'];

    if ($_POST['function'] == 'Stop Server') {
        $stop_msg = $_POST['msg'];

        $return_msg = $tsAdmin->serverStop($_COOKIE['virtualserver_id'], $stop_msg);
        echo json_encode($return_msg);
        return;
    }

    if ($_POST['function'] == 'Start Server') {
        $return_msg = $tsAdmin->serverStart($_COOKIE['virtualserver_id']);
        echo json_encode($return_msg);
        return;
    }

    if ($_POST['function'] == 'Server_Status') {
        $server_info = $tsAdmin->serverInfo();
        echo json_encode($server_info);
        exit;
    }

    if ($_POST['function'] == 'Global_Message') {
        if (!$_POST['message']) return;
        $message = $_POST['message'];
        $tsAdmin->gm($message);
    }

    if ($_POST['function'] == 'Get_DB_Client_List') {
        $clients = $tsAdmin->clientDbList(0, 99999)["data"];

        for ($i = 0; $i < count($ignore_cdbids); $i++)  {
            foreach ($clients as $key => $value) {
                if ($clients[$key]["cldbid"] == $ignore_cdbids[$i]) unset($clients[$key]);
            }     
        }
        
        $clients = array_values($clients);
        echo json_encode($clients);
        exit;
    }

    if ($_POST['function'] == 'Get_Client_List') {
        $clients = $tsAdmin->clientList()["data"];
        // filter out serveradmin
        foreach ($clients as $key => $value) {
            if ($clients[$key]["client_nickname"] == "serveradmin") unset($clients[$key]);
        }
        // Unset, transforms array into object, array values fixes this bug.
        $clients = array_values($clients);
        echo json_encode($clients);
        exit;
    }
    
    if ($_POST['function'] == 'Get_Client_Server_Groups') {
        $client_dbid = $_POST['client_dbid'];
        if (!$client_dbid) return;
        //$tsAdmin->selectServer(1, "serverId");
        $client_server_groups = $tsAdmin->serverGroupsByClientID($client_dbid);
        echo json_encode($client_server_groups['data']);
        exit;
    }

    if ($_POST['function'] == 'Get_Server_Groups') {
        $server_groups = $tsAdmin->serverGroupList(1)["data"];
        foreach ($server_groups as $key => $sg) {
            if (in_array($sg["sgid"], $ignore_sg_list)) {
                unset($server_groups[$key]);
            } 
        }
        echo json_encode($server_groups);
        exit;
    }

    if ($_POST['function'] == 'Get_All_Server_Group_Icons') {
        // Init cache manager class
        $cache_manager = new Cache_Manager();
        if ($cache_manager->can_update_cache()) {
            $array = array();
            $server_groups = $tsAdmin->serverGroupList(1)["data"];   
            foreach ($server_groups as $server_group) {
                $array[$server_group["sgid"]] = $tsAdmin->getIconByID($server_group["iconid"])["data"]; 
            }
            $cache_manager->server_group_icons = $array;
            $cache_manager->set_cache();
        }
        $icons_cache = $cache_manager->get_cache();
        $icons_cache->null_img = base64_encode(file_get_contents(NULL_ICON_PATH));
        echo json_encode($icons_cache);
        exit;
    }

    if ($_POST['function'] == 'Add_SG') {
        $clbid = $_POST['client_dbid'];
        $sgid = $_POST['sgid'];
        //$tsAdmin->selectServer(1, "serverId");
        $server_groups = $tsAdmin->serverGroupAddClient($sgid, $clbid);
        //echo json_encode($server_groups);
        exit;
    }

    if ($_POST['function'] == 'Remove_SG') {
        $clbid = $_POST['client_dbid'];
        $sgid = $_POST['sgid'];
        //$tsAdmin->selectServer(1, "serverId");
        $server_groups = $tsAdmin->serverGroupDeleteClient($sgid, $clbid);
        //echo json_encode($server_groups);
        exit;
    }

    if ($_POST['function'] == 'Ban_Client') {
        // Class Ban props: numeric, type, format, ban, recipient
        $ban = json_decode($_POST['ban']);
        $client = $tsAdmin->clientDbInfo($ban->recipient)["data"];
        if ($ban->type == "IP") {
            $id = $tsAdmin->banAddByIp($client["client_lastip"], $ban->numeric*$ban->format, $ban->reason);
        } elseif ($ban->type == "UID") {
            $id = $tsAdmin->banAddByUid($client["client_unique_identifier"], $ban->numeric*$ban->format, $ban->reason);
        }
        echo json_encode($id);
        exit;
    }

    if ($_POST['function'] == 'Kick_Client') {
        // Class Kick props: int $cdbid recipient, string $reason
        $kick = json_decode($_POST['kick']);
        $client_list = $tsAdmin->clientList()["data"];
        foreach ($client_list as $client) {
            if ($client["client_database_id"] == $kick->recipient) {
                $clid = $client["clid"];
                break;
            }
        }

    if (empty($clid)) exit;

        $kick_return = $tsAdmin->clientKick($clid, "server", $kick->reason);
        echo json_encode($kick_return);
        exit;
    }

    if ($_POST['function'] == 'Message_Client') {

        /**
        * Object Message
        * @property   string $type    Chat/Poke
        * @property   string $content	"text"
        * @property   integer $recipient cdbid
        */

        $message = json_decode($_POST['message']);
        $client_list = $tsAdmin->clientList()["data"];
        foreach ($client_list as $client) {
            if ($client["client_database_id"] == $message->recipient) {
                $clid = $client["clid"];
                break;
            }
        }

    if (empty($clid)) exit;
        
        if ($message->type == "Chat") {
            $return_code = $tsAdmin->sendMessage(1, $clid, $message->content);
        } elseif ($message->type == "Poke") {
            $return_code = $tsAdmin->clientPoke($clid, $message->content);
        }

        echo json_encode($return_code);
        exit;
    }

    if ($_POST['function'] == 'Get_Ban_List') {
        $ban_list = $tsAdmin->banList();

        echo json_encode($ban_list);
        exit;
    }

    if ($_POST['function'] == 'Delete_Ban_Rule') {
        $ban_id = json_decode($_POST['ban_id']);
        $return_status = $tsAdmin->banDelete($ban_id);

        echo json_encode($return_status);
        exit;
    }

    if ($_POST['function'] == 'Ban_by_IP') {
        $ban = json_decode($_POST['ban']);
        $ban_response = $tsAdmin->banAddByIp($ban->recipient, $ban->duration, $ban->reason);
        echo json_encode($ban_response);
        exit;
    }

    if ($_POST['function'] == 'Ban_by_UID') {
        $ban = json_decode($_POST['ban']);
        $ban_response = $tsAdmin->banAddByUid($ban->recipient, $ban->duration, $ban->reason);
        echo json_encode($ban_response);
        exit;
    }

    if ($_POST['function'] == 'Ban_by_Name') {
        $ban = json_decode($_POST['ban']);
        $ban_response = $tsAdmin->banAddByName($ban->recipient, $ban->duration, $ban->reason);
        echo json_encode($ban_response);
        exit;
    }
    
?>
